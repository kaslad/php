<?php
/**
 *
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 18.03.18
 * Time: 0:14
 */


include "index.html";
include "script.php";
$input = "";
$amountOfItersToCheck = 10000;
$amountsOfStringToBeGen = 0;
$n = 0;
if(isset($_GET["input"])){
    $input = $_GET["input"];

    $array = explode("\n", $input);
    $n = count($array);

    for($i = 0; $i < $n; $i++){
        $arr = $array[$i];
        $arr = trim($arr);
        $string = preg_split("/[\s]+/", $arr);
        //weights
        $weights[$i] = $string[count($string) - 1];
        $fixedString[$i] = "";
        for ($j = 0; $j < count($string) - 1; $j++){
            $fixedString[$i] = $fixedString[$i] . " " . $string[$j];
        }
    }
    //1 task

    $sum = 0;

    for($i = 0; $i < $n; $i++){
        $sum += $weights[$i];
    }
    for($i = 0; $i < $n; $i++) {
        $stringInfo[$i] = [
            "text" => $fixedString[$i],
            "weight" => $weights[$i],
            "probability" => $weights[$i] / $sum
        ];
    }
    $firstOut = [
        "sum" => $sum,
        "data" => $stringInfo
    ];

    // 2 task
    $step = 0;
    for($i = 0; $i < $n; $i++){
        for($q = 0; $q < $weights[$i]; $q++){
            $strings[$q + $step] = $i;
        }
        $step += $weights[$i];
    }
    $amountsOfStringToBeGen = checker($strings);

    for($i = 0; $i < $n; $i++){
        $secondOut[$i] = [
            "text" => $fixedString[$i],
            "count" => $amountsOfStringToBeGen[$i],
            "calculated_probability" => $amountsOfStringToBeGen[$i] / $amountOfItersToCheck,
        ];
    }
    print "1 task<br>";
    print_r(json_encode($firstOut, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
    print "<br>2 task<br>";
    print_r(json_encode($secondOut, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));

}







?>
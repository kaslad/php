<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 22.04.18
 * Time: 19:46
 */

spl_autoload_register(function ($class_name) {
    echo ($class_name);
    str_replace("\\", "/", $class_name);
    include $class_name . '.php';
});
include "index.html";

$input = $_GET["input"]??"none";

$input = explode("\n", $input);

$name = trim($input[0]??"none"); //user name
$health = trim($input[1]??"none"); //здоровье
$power = trim($input[2]??"none"); //сила
$side = trim($input[3]??"none"); //сторона за которую user играет(bad или good)
$heroName = trim($input[4]??"none");//имя персонажа

$exceptionUser = new ExceptionUser($name, $health, $power, $side, $heroName);
//проверка здоровья user на валидность(здоров ли еще либо верхняя граница не преувеличена)
try {
    $exceptionUser->getHealth();
} catch (\MySpace\ExceptionFifth $e) {
    echo ' Исключение: ',  $e->getMessage(), "<br>";

} catch (\MySpace\ExceptionSecond $e) {
    echo ' Исключение: ',  $e->getMessage(), "<br>";

}
//проверка силы удара user на валидность
try {
    $exceptionUser->getPower();
} catch (MySpace\ExceptionFifth $e) {
    echo ' Исключение: ',  $e->getMessage(), "<br>";

} catch (MySpace\ExceptionFirst $e) {
    echo ' Исключение: ',  $e->getMessage(), "<br>";

}
//проверка команды за которую играет user(good или bad)

try {
    $exceptionUser->getSide();
} catch (MySpace\ExceptionFifth $e) {
    echo ' Исключение: ',  $e->getMessage(), "<br>";

} catch (MySpace\ExceptionThird $e) {
    echo ' Исключение: ',  $e->getMessage(), "<br>";

}
//провека персонажа за которого играет user
try {
    $exceptionUser->getHeroName();
} catch (MySpace\ExceptionFifth $e) {
    echo ' Исключение: ',  $e->getMessage(), "<br>";

} catch (MySpace\ExceptionFourth $e) {
    echo ' Исключение: ',  $e->getMessage(), "<br>";

}
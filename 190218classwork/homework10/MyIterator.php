<?php

class MyIterator implements Iterator
{
	
    private $array = array();
	private $flag = "activ";
	private $day;
	private $curMonth;
	
	public function __construct(String $day, String $curMonth){
		$this->array = range(1, 31);
		$this->day = $day;
		$this->curMonth = $curMonth;

    }

    public function rewind()
    {
        reset($this->array);
    }
  
    public function current()
    {

        $var = current($this->array);
        $day = date("w", strtotime($this->curMonth . $var));
        $string = "";
        if ($this->flag == "activ"){
            $string = "";
            for ($i = 0; $i < $day - 1; $i++)
                $string = $string  . "&nbsp&nbsp&nbsp";
            $this->flag = "disactiv";
        }

        $var = key($this->array);
        if ($var < 10)
            $string = $string  . "&nbsp";

        if ($day == 0)
            return $string . $var . "<br>";
        else
            return $string . $var . "&nbsp";

    }
  
    public function key() 
    {
        $var = key($this->array);

    }
  
    public function next() 
    {
        $var = next($this->array);
        return $var;
    }
  
    public function valid()
    {
        $key = key($this->array);
        $var = ($key !== NULL && $key !== FALSE);
        return $var;
    }

}
?>
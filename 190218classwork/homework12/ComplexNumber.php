<?php

class ComplexNumber
{
	
	public $act;
	public $img;
	
	function __construct($actual, $imaginary) {
		$this->act = $actual;
		$this->img = $imaginary;
	}
	
	public function getAct(){
		return $this->act;
	}
	
	public function getImg(){
		return $this->img;
	}
	
	public function add(ComplexNumber $complex){
		$act = $this->act + $complex->getAct();
		$img = $this->img + $complex->getImg();
		return new ComplexNumber($act, $img);
	}
    public function mult(ComplexNumber $complex){
        $act = $this->act * $complex->getAct() - $this->img * $complex->getImg();
        $img = $this->act * $complex->getImg() + $complex->getAct() * $this->img;
        return new ComplexNumber($act, $img);

    }

    public function div(ComplexNumber $complex){
        $act = ($this->act * $complex->getAct() + $this->img * $complex->getImg()) / ($complex->getAct() * $complex->getAct() + $complex->getImg() * $complex->getImg());
        $img = ($complex->getAct() * $this->img - $this->act * $complex->getImg()) / ($complex->getAct() * $complex->getAct() + $complex->getImg() * $complex->getImg());
        return new ComplexNumber($act, $img);
    }
	
	public function sub(ComplexNumber $complex){
		$act = $this->act - $complex->getAct();
		$img = $this->img - $complex->getImg();
		return new ComplexNumber($act, $img);
	}


}

?>
<?php

require_once 'ComplexNumber.php';

$c1 = new ComplexNumber(5, 3);
$c2 = new ComplexNumber(2, 4);
$c3 = $c1->mult($c2);

echo $c3->getAct() . " + ";
echo $c3->getImg() . "*i";


?>
<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 25.03.18
 * Time: 18:44
 */

include "index.html";

$input = $_GET["input"];

if (!preg_match('/^.{10,}/', $input, $matches)){
    echo "в пароле меньше 10 символов" . "<br>";
}
if (!preg_match('/[0-9].*[0-9]/', $input, $matches)){
    echo "пароль не содержит 2 различных цифр" . "<br>";
}
if (!preg_match('/[%$#_*].*[%$#_*]/', $input, $matches)){
    echo "пароль не содержит 2 различных символов из %$#_*" . "<br>";
}
if (!preg_match('/[a-z].*[a-z]/', $input, $matches)){
    echo "пароль не содержит 2 прописных различных латинских символов" . "<br>";
}
if (!preg_match('/[A-Z].*[A-Z]/', $input, $matches)){
    echo "пароль не содержит 2 заглавных различных латинских символов" . "<br>";
}
if (preg_match('/[A-Z]{4,}/', $input, $matches)){
    echo "пароль содержит более 3 заглавных различных буквы подряд" . "<br>";
}
if (preg_match('/[a-z]{4,}/', $input, $matches)){
    echo "пароль содержит более 3 прописных различных буквы подряд" . "<br>";
}
if (preg_match('/[0-9]{4,}/', $input, $matches)){
    echo "пароль содержит более 3 латинских различных цифры подряд" . "<br>";
}
if (preg_match('/[%$#_*]{4,}/', $input, $matches)){
    echo "пароль содержит более 3 различных %$#_* подряд". "<br>";
}


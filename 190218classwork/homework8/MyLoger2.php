<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 14.04.18
 * Time: 14:48
 */
require_once 'MyLogerInterface.php';

class MyLoger2 extends MyLogerInterface
{
		
	private $timeFormat;
	private $off;

	
	public function func(String $text){
	    if($this->off){
            echo $this->timeFormat. "<br>";

        }
        else{
            echo date($this->timeFormat) . "<br>";

        }
		echo $text . "<br>";
	}


    function __construct(String $time, bool $off) {
	    $this->off = $off;
        $this->timeFormat = $time;
    }
}
?>
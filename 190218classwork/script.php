<?php
$code=$_POST["code"];
$input=$_POST["input"];
$j = 0;
$inputPosition = 0;
$storage[$j] = 0;
$answer = "";
$jMax = 0;

for($i = 0; $i < strlen($code); $i++){
	if ($code[$i] == '>'){
		$j++;
		$jMax = max($jMax, $j);
		if($j > $jMax){
		    $storage[$j] = 0;
		    $jMax = $j;
        }
	}
	if ($code[$i] == '<'){
		$j--;
	}
    if ($code[$i] == '-'){
        if(!isset($storage[$j])){
            $storage[$j] = 0;
        }
        $storage[$j]--;
        if ($storage[$j] < 0) {
            $storage[$j] = 255;
        }
    }
	if ($code[$i] == '+'){
	    if(!isset($storage[$j])){
            $storage[$j] = 0;
        }
		$storage[$j]++;
		if ($storage[$j] > 255) {
            $storage[$j] = 0;
        }
	}
    if ($code[$i] == ','){
        $storage[$j] = ord($input[$inputPosition]);
        $inputPosition++;
    }
	if ($code[$i] == '.'){
        if(!isset($storage[$j])){
            $storage[$j] = 0;
        }
		$answer = $answer . chr($storage[$j]);
	}

	if ($code[$i] == '['){
		if ($storage[$j] == 0){
			$countBrackets = 1;
			while ($countBrackets != 0){
				$i++;
				if ($code[$i] == ']'){
					$countBrackets--;
				}
				if ($code[$i] == '['){
					$countBrackets++;
				}
			}
		}
	}
	
	if ($code[$i] == ']'){
		if ($storage[$j] != 0){
			$countBrackets = 1;
            while ($countBrackets != 0) {
                $i--;
                if ( $code[ $i ] == '[' ) {
                    $countBrackets--;
                }
                if ( $code[ $i ] == ']' ) {
                    $countBrackets++;
                }
			}
		}

	}
} 
echo $answer;
?>
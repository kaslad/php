<?php

require_once 'MyIterator.php';

class Month implements IteratorAggregate {
    private $day;
    private $curMonth = "2017-12-";
	public function __construct(String $day){
        $this->day = $day;
    }
    public function getIterator() {
		$myIterator = new MyIterator($this->day, $this->curMonth);
		return $myIterator;
    }
    public function getWeekDay(){
        $day = strftime(" %a", strtotime($this->curMonth . $this->day));
        return $day;

    }
}
?>


<?php
include "index.html";
$ini = basename(__FILE__);
$ini = substr($ini, 0, -3);
$ini = $ini . "ini";

$arr = parse_ini_file($ini, true);
$file = fopen($arr["main"]["filename"], "r");


$firstRule = $arr["first_rule"]["symbol"];
$secondRule = $arr["second_rule"]["symbol"];
$thirdRule = $arr["third_rule"]["symbol"];
$fUpper = $arr["first_rule"]["upper"];
$sDir = $arr["second_rule"]["direction"];
$tClear = $arr["third_rule"]["delete"];


while(!feof($file)) {
    $string = fgets($file);
    if (stripos($string, $firstRule) === 0){
        if ($fUpper == "true"){
            $string = mb_strtoupper($string);
        }
        else{
            $string = mb_strtolower($string);
        }
        echo $string  . "<br>";
        continue;
    }
    if (stripos($string, $secondRule) === 0){
        if ($sDir == "+"){
            for ($i = 0; $i < strlen($string); $i++){
                if(is_numeric($string[$i]))
                $string[$i] = ($string[$i] + 1) % 10;
            }
        }
        else{
            if(is_numeric($string[$i]))
            $string[$i] = 0 ? "9" : $string[$i] - 1;

        }
        echo $string . "<br>";
        continue;
    }
    if (stripos($string, $thirdRule) === 0){
        $string = str_replace($tClear, "", $string);
        echo $string  . "<br>";
        continue;
    }
    echo $string;

}


fclose($file);





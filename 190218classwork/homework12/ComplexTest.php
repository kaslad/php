<?php

require_once 'ComplexNumber.php';
use PHPUnit\Framework\TestCase;

class ComplexTest extends TestCase {
    
	public function testAdd()
    {
        $testComplex1 = new ComplexNumber(3, 2);
		$testComplex2 = new ComplexNumber(4, 1);
        $this->assertEquals(7, $testComplex1->add($testComplex2)->getAct());
		$this->assertEquals(3, $testComplex1->add($testComplex2)->getImg());
	}

	public function testSub()
    {
        $testComplex1 = new ComplexNumber(2, 2);
		$testComplex2 = new ComplexNumber(4, 1);
        $this->assertEquals(-2, $testComplex1->sub($testComplex2)->getAct());
		$this->assertEquals(1, $testComplex1->sub($testComplex2)->getImg());
	}

	public function testMult()
    {
        $testComplex1 = new ComplexNumber(2, 2);
		$testComplex2 = new ComplexNumber(4, 1);
        $this->assertEquals(6, $testComplex1->mult($testComplex2)->getAct());
		$this->assertEquals(10, $testComplex1->mult($testComplex2)->getImg());
	}

	public function testDiv()
    {
        $testComplex1 = new ComplexNumber(3, 2);
		$testComplex2 = new ComplexNumber(2, -1);
        $this->assertEquals(0.8, $testComplex1->div($testComplex2)->getAct());
		$this->assertEquals(1.4, $testComplex1->div($testComplex2)->getImg());
	}
	
}
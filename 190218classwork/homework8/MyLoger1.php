<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 14.04.18
 * Time: 14:47
 */
require_once 'MyLogerInterface.php';

class MyLoger1 extends MyLogerInterface
{

    private $file;

    public function func(String $text){
        $fileOpen = fopen($this->file, 'a+');
        fwrite($fileOpen, $text . PHP_EOL);
        fclose($fileOpen);
    }
    function __construct(String $file) {
        $this->file = $file;
    }

}
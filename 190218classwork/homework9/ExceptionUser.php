<?php
/**
 * Created by PhpStorm.
 * User: Vladislav
 * Date: 22.04.18
 * Time: 19:55
 */

class ExceptionUser
{
    private $name;
    private $health;
    private $power;
    private $side;
    private $heroName;

    private $heroes = ["mag", "pudge", "traxex", "king"];
    function __construct($name, $health, $power, $side, $heroName)
    {
        $this->name = $name;
        $this->health = $health;
        $this->power = $power;
        $this->side = $side;
        $this->heroName = $heroName;
    }

    public function getPower()
    {
        if ($this->power < 20) {
            throw new MySpace\ExceptionFirst("Power must be more than 20");
        }
        if (preg_match('/([0-9])/', $this->name, $matches)){
            throw new MySpace\ ExceptionFifth("The name has numbers");
        }
        return $this->name . " power: ".$this->power . "<br>";
    }

    public function getHealth()
    {
        if ($this->health < 20) {
            throw new MySpace\ExceptionSecond("Health must be between 0 and 100");
        }
        if (preg_match('/([0-9])/', $this->name, $matches)){
            throw new MySpace\ExceptionFifth("The name has numbers");
        }
        return $this->name . " power: " . $this->health . "<br>";

    }

    public function getSide (){
        if(strtolower($this->side) != "bad" && strtolower($this->side != "good")){
            throw new MySpace\ExceptionThird("Side must be bad or good");

        }
        if (preg_match('/([0-9])/', $this->name, $matches)){
            throw new MySpace\ExceptionFifth("The name has numbers");
        }
        return $this->name . " power: " . $this->health . "<br>";

    }

    public function getHeroName(){

        if(!in_array(strtolower($this->heroName),$this->heroes)){
            throw new MySpace\ExceptionFourth("Not a real hero Name");
        }
        if (preg_match('/([0-9])/', $this->name, $matches)){
            throw new MySpace\ExceptionFifth("The name has numbers");
        }
        return $this->name . " hero: " . $this->health . "<br>";

    }
}